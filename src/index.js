import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { AuthContext } from './util/AuthService';

ReactDOM.render(
  <AuthContext.Provider value={{ isAuthenticated: false }}>
    <App />
  </AuthContext.Provider>,
  document.getElementById('root')
);
