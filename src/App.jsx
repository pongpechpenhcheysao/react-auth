import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Home from './pages/Home';
import Products from './pages/Products';
import Blog from './pages/Blog';
import Login from './pages/Login';
import ProductDetailsPage from './pages/ProductDetailPage';
import { Layout } from './layout/Layout';

const App = () => {
  const [token, setToken] = useState(!!localStorage.getItem('token'));
  const [userData, setUserData] = useState(null);
  const [products, setProducts] = useState([]);

  const handleLogin = (user) => {
    setToken(true);
    setUserData(user);
  };

  return (
    <div>
      <Router>
        <Routes>
          {token ? (
            <Route path="/" element={<Layout userData={userData} />}>
              <Route index element={<Home userData={userData} />} />
              <Route path="blog" element={<Blog />} />
              <Route path="product" element={<Products setProducts={setProducts} />} />
              <Route path="product/:Id" element={<ProductDetailsPage  />} />
              <Route path="*" element={<Navigate to="/" />} />
            </Route>
          ) : (
            <>
              <Route index path="auth/login" element={<Login onLogin={handleLogin} />} />
              <Route path="*" element={<Navigate to="/auth/login" />} />
            </>
          )}
          <Route path="error" element={<h1>Error</h1>} />
        </Routes>
      </Router>
    </div>
  );
};

export default App;