import CardDetails from '../components/CardDetail';
import { useParams, Link } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const ProductDetailsPage = () => {
  const  productId  = useParams().Id;
  const [product, setProduct] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get(`https://dummyjson.com/products/${productId}`)
      .then((response) => {
        setProduct(response.data);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
        setError(error);
      });
  }, []);

  return (
    <div style={{backgroundColor:'rgb(234,236,243)', paddingBottom:'100px'}}>
      <CardDetails product={product}/>
    </div>
  );
};

export default ProductDetailsPage;