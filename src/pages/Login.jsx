import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css';

const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      navigate('/home');
    }
    
  }, [navigate]);

  const handleLogin = () => {
    axios
      .post('https://dummyjson.com/auth/login', {
        username: username,
        password: password,
      }, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(response => {
        localStorage.setItem('token', response.data.token);

        const userData = response.data.user;

        onLogin(userData);
        setError('');
      })
      .catch(error => {
        setError(error.response.data.message);
      });
  };

  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100vh',
      backgroundImage: 'url(https://static-cse.canva.com/blob/1210563/10experttipsfordesigningwithablurredbackgroundfeaturedimage.jpg)',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    }}>
      <div className='bg-light text-center' style={{ height: '300px', width: '300px' }}>
        <h2 className='pt-5'><b>Login</b></h2>
        <div className='pt-3'>
          <input type="text" placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)} />
        </div>
        <div className='py-4'>
          <input type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
        </div>
        <button className='loginBtn' onClick={handleLogin}>Login</button>
        <p className="text-danger pt-3">{error}</p>
      </div>
    </div>
    
  );
};

export default Login;