import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Card from '../components/Card';
import ProductCategories from '../components/ProductCategories';
import Pagination from '../components/ProductPagination';
import ProductSearch from '../components/ProductSearch';

const Products = () => {
  const [allProducts, setAllProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const productsPerPage = 12;

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await axios.get('https://dummyjson.com/products?limit=100');
        let productsData = response.data.products;

        setAllProducts(productsData);
        setFilteredProducts(productsData);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setError(error);
        setLoading(false);
      }
    };

    fetchProducts();
  }, []);

  useEffect(() => {
    const handleCategorySelect = async (categoryId) => {
      try {
        const response = await axios.get(`https://dummyjson.com/products/category/${categoryId}?limit=100`);
        const categoryData = response.data;

        if (categoryData && Array.isArray(categoryData.products)) {
          setFilteredProducts(categoryData.products);

          setCurrentPage(1);
          const totalPages = Math.ceil(categoryData.products.length / productsPerPage);
          setCurrentPage(totalPages);
        } else {
          console.error('Invalid category data format:', categoryData);
          setFilteredProducts([]);
        }
      } catch (error) {
        console.error('Error fetching category products:', error);
        setError(error);
      }
    };

    if (selectedCategory) {
      handleCategorySelect(selectedCategory);
    } else {
      setFilteredProducts(allProducts);
    }
  }, [selectedCategory, allProducts, productsPerPage]);

  const handleSearchResults = (results) => {
    setSearchResults(results);
    setFilteredProducts(results);
    setCurrentPage(1);
  }; 

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const indexOfLastProduct = currentPage * productsPerPage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProducts = filteredProducts.slice(indexOfFirstProduct, indexOfLastProduct);
  const totalPages = Math.ceil(filteredProducts.length / productsPerPage);

  return (
    <div style={{ backgroundColor: 'rgb(234,236,243)', paddingBottom: '100px' }}>
      <h2 className="pb-5 text-center navColor2" style={{ paddingTop: '75px' }}>
        Welcome to Product page
      </h2>

      <div className="container mt-4">
        {loading ? (
          <h5 className='text-center navColor2'><b>Loading</b></h5>
        ) : error ? (
          <p>Error fetching products: {error.message}</p>
        ) : (
          <div>
            <div className="row">
              <div className="col-md-3">
                <ProductCategories onSelectCategory={setSelectedCategory} />
              </div>
              <div className="col-md-9">
                <ProductSearch onSearchResults={handleSearchResults} />

                {currentProducts.length === 0 ? (
                  <h5 className='text-center navColor2'>There are no product.</h5>
                ) : (
                  <div className="row row-cols-1 row-cols-md-3">
                    {currentProducts.map((product) => (
                      <div key={product.id} className="col mb-4">
                        <Link to={`/product/${product.id}`}>
                          <Card product={product} />
                        </Link>
                      </div>
                    ))}
                  </div>
                )}
                <div className='py-4' style={{display:'flex', justifyContent:'center'}}>
                  <Pagination class currentPage={currentPage} totalPages={totalPages} onPageChange={handlePageChange} />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Products;