import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../App.css';

const ProductCategories = ({ onSelectCategory, currentPage }) => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const response = await axios.get(`https://dummyjson.com/products/categories?page=${currentPage}`);
        const categoriesData = response.data;

        setCategories(categoriesData);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching categories:', error);
        setError(error);
        setLoading(false);
      }
    };

    fetchCategories();
  }, [currentPage]);

  const handleAllClick = () => {
    onSelectCategory(null);
  };

  const handleCategoryClick = (category) => {
    onSelectCategory(category);
  };

  return (
    <div>
      <h3>Product Categories</h3>
      {loading ? (
        <h5 className='navColor2'>Loading</h5>
      ) : error ? (
        <p>Error fetching categories: {error.message}</p>
      ) : (
        <div>
          <li className='py-1'>
            <button className={`catBtn ${currentPage === 0 ? 'active' : ''}`} onClick={handleAllClick}>
              All
            </button>
          </li>
          {categories.map((category) => (
            <li className='py-1' key={category}>
              <button
                className={`catBtn ${currentPage !== 0 && onSelectCategory === null ? 'active' : ''}`}
                onClick={() => handleCategoryClick(category)}
              >
                {category}
              </button>
            </li>
          ))}
        </div>
      )}
    </div>
  );
};

export default ProductCategories;
