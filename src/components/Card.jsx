import React, { useState } from 'react';
import CardDetails from './CardDetail';

const Card = ({ product }) => {
const [showDetails, setShowDetails] = useState(false);

const handleClick = () => {
    setShowDetails(true);
};

const handleClose = () => {
    setShowDetails(false);
};

return (
    <>
    <div className="card" onClick={handleClick}>
        <img
        src={product.thumbnail}
        alt={product.title}
        className="card-img-top"
        onError={(e) => {
            e.target.onerror = null;
            e.target.src = "fallback_image_url";
        }}
        />
        <div className="card-body">
        <h5 className="card-title">{product.title}</h5>
        <p className="card-text">Price: {product.price}$</p>
        </div>
    </div>

    {showDetails && <CardDetails product={product} onClose={handleClose} />}
    </>
);
};

export default Card;
