import React from 'react';

const CardDetails = ({ product }) => {
  return (
    <section className="pt-5">
    <div className="container py-5">
      <div className="row">
        <div className="col-lg-6 col-md-12">
          <div className="card p-3" style={{ width: '500px' }}>
            <img src={product.thumbnail} className="card-img-top" style={{height: '250px'}} alt="foodimage" />
            <div className="card-body">
              <h5 className="card-title pt-3"><b>{product.title}</b></h5>
              <div className="rating">
                <button className="btn btn-success">{product.rating} &#9733;</button>
              </div>
              <div className="py-3">
              <p className="card-text">Price: {product.price}$</p>
              <p className="card-text">Stock: {product.stock} left</p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-12">
          <h3 className="text-center pt-1 pb-3">Detail</h3>
          <div className="pb-3">
            <ul className="px-5">
              <p className="card-text"><b>Description:</b> {product.description}</p>
              <p className="card-text"><b>Brand:</b> {product.brand}</p>
              <p className="card-text"><b>Category:</b> {product.category}</p>
        
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  );
};

export default CardDetails;