import React, { useState } from 'react';
import axios from 'axios';

const ProductSearch = ({ onSearchResults }) => {
  const [searchInput, setSearchInput] = useState('');

  const handleSearchChange = (e) => {
    setSearchInput(e.target.value);
  };

  const handleSearchSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.get(`https://dummyjson.com/products/search?q=${searchInput}&limit=100`);
      const searchData = response.data;

      if (searchData && Array.isArray(searchData.products)) {
        onSearchResults(searchData.products);
      } else {
        console.error('Invalid search data format:', searchData);
        onSearchResults([]);
      }
    } catch (error) {
      console.error('Error fetching search results:', error);
      onSearchResults([]);
    }
  };

  return (
    <form onSubmit={handleSearchSubmit} className="mb-3">
      <label>
        <input
          className='searchForm'
          type="text"
          value={searchInput}
          onChange={handleSearchChange}
          placeholder='SEARCH'
        />
      </label>
      <button className='searchBtn' type="submit">Search</button>
    </form>
  );
};

export default ProductSearch;