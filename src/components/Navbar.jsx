import React from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

const Navbar = ({ handleSearchChange, handleSearchSubmit }) => {
  const handleLogout = () => {
    localStorage.removeItem('token');
    window.location.reload();
  };

  return (
    <nav className="navbar navbar-expand-lg bg-body-tertiary">
      <div className="container-fluid">
        <Link to="/" className="navbar-brand ps-3"><h3>Store</h3></Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link to="/" className="nav-link ps-5" aria-current="page"><b className='navColor'>Home</b></Link>
            </li>
            <li className="nav-item">
              <Link to="/product" className="nav-link px-4" aria-current="page"><b className='navColor'>Product</b></Link>
            </li>
            <li className="nav-item">
              <Link to="/blog" className="nav-link px-4" aria-current="page"><b className='navColor'>Blog</b></Link>
            </li>
          </ul>
        </div>
        <button className='loginBtn' onClick={handleLogout}><b className='navColor'>Logout</b></button>
      </div>
    </nav>
  );
};

export default Navbar;
